# New Changelogs

* Added new input -u extra_data to add user and other custom data in license hash

* fix in verify license after extra_data

* Cmake File changes to make open-license-manager as a installable dev build ( Module it in such a way that licence generator can be embedded in any other product easily without compiling open-license-manager (compilation of this would lead to update license client with generator))


* install.sh added for easy setup
